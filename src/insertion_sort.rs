use std::cmp::Ord;
use super::generic_sort::Sortable;

pub struct InsertionSort<T: Ord> {
    storage: Vec<T>
}

impl<T: Ord + Clone> Sortable<T> for InsertionSort<T> {
    fn sort(&mut self) -> &Vec<T> {
        for current_position in 1 .. self.storage.len() {
            // current_position contains the position in the array **decreased by 1**
            for backward_position in (0 ..= current_position).rev() {
                if self.storage[backward_position + 1] < self.storage[backward_position] {
                    let tmp = self.storage[backward_position].clone();
                    self.storage[backward_position] = self.storage[backward_position + 1].clone();
                    self.storage[backward_position + 1] = tmp;
                }
            }
        }
        &self.storage
    }
    fn new(list: &Vec<T>) -> Self {
        Self{storage: list.clone()}
    }
}
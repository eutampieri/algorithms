use std::cmp::Ord;
use super::generic_sort::Sortable;

pub struct Rust<T: Ord> {
    storage: Vec<T>
}

impl<T: Ord + Clone> Sortable<T> for Rust<T> {
    fn sort(&mut self) -> &Vec<T> {
        self.storage.sort();
        &self.storage
    }
    fn new(list: &Vec<T>) -> Self {
        Self{storage: list.clone()}
    }
}

pub struct RustUnstable<T: Ord> {
    storage: Vec<T>
}

impl<T: Ord + Clone> Sortable<T> for RustUnstable<T> {
    fn sort(&mut self) -> &Vec<T> {
        self.storage.sort();
        &self.storage
    }
    fn new(list: &Vec<T>) -> Self {
        Self{storage: list.clone()}
    }
}

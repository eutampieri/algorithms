use rand::prelude::*;

mod generic_sort;
use generic_sort::*;

mod insertion_sort;
mod rust_sort;
mod merge_sort;

static MIN_VALUE: i64 = -1_000_000;
static MAX_VALUE: i64 = 1_000_000;
static NUM_ELEMENTS: usize = 1_000_000;
static ROUNDS: u32 = 100;

fn print_result(res: (f64, f64, f64, usize)) {
    println!("\nRan insertion sort on {} different inputs each {} items long ranging from {} to {}.\nThe algorithm was wrong {} times.", ROUNDS, NUM_ELEMENTS, MIN_VALUE, MAX_VALUE, res.3);
    println!("Statistics: min = {} ms, max = {} ms, avg = {} ms", res.0, res.1, res.2);
}

fn main() {
    println!("Generating inputs");
    let mut rng = rand::thread_rng();
    let inputs: Vec<Vec<i64>> = (0 .. ROUNDS).map(|_| (0 .. NUM_ELEMENTS).map(|_| rng.gen_range(MIN_VALUE, MAX_VALUE + 1)).collect()).collect();
    if NUM_ELEMENTS <= 10_000 {
        println!("Testing insertion sort");
        print_result(SortAlgoTester::<insertion_sort::InsertionSort<i64>>::get_times(&inputs));
    } else {
        println!("Not testing insertion sort, run with INPUT_SIZE_OVERRIDE to run it anyway");
    }
    
    println!("Testing Rust's builtin sort");
    print_result(SortAlgoTester::<rust_sort::Rust<i64>>::get_times(&inputs));
    
    println!("Testing Rust's builtin unstable sort");
    print_result(SortAlgoTester::<rust_sort::RustUnstable<i64>>::get_times(&inputs));
    
    println!("Testing merge sort");
    print_result(SortAlgoTester::<merge_sort::MergeSort<i64>>::get_times(&inputs));
}

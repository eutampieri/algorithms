pub trait Sortable<T: Ord> {
    fn sort(&mut self) -> &Vec<T>;
    fn new(list: &Vec<T>) -> Self;
}

pub struct SortAlgoTester<T: Sortable<i64>>{
    _algo: T
}

fn duration_into_ms(d: std::time::Duration) -> f64 {
    d.as_secs() as f64 * 1000f64 + d.subsec_nanos() as f64 / 1_000_000f64
}

impl<T: Sortable<i64>> SortAlgoTester<T> {
    fn check_sorting(sorted_list: &Vec<i64>) -> bool {
        let mut last = &sorted_list[0];
        for element in sorted_list.iter().skip(1) {
            if element < last {
                return false;
            }
            last = element;
        }
        true
    }
    pub fn get_times(inputs: &Vec<Vec<i64>>) -> (f64, f64, f64, usize){
        let mut min = std::f64::MAX;
        let mut max = 0f64;
        let mut avg = 0f64;
        let mut wrong = 0usize;
        let mut pb = pbr::ProgressBar::new(inputs.len() as u64);
        pb.format("╢▌▌░╟");
        for input in inputs {
            let mut instance = T::new(input);
            let start = std::time::SystemTime::now().duration_since(std::time::UNIX_EPOCH).unwrap();
            let result = instance.sort();
            let duration = duration_into_ms(std::time::SystemTime::now().duration_since(std::time::UNIX_EPOCH).unwrap() - start);
            if SortAlgoTester::<T>::check_sorting(result) {
                if duration < min {
                    min = duration;
                }
                if duration > max {
                    max = duration;
                }
                avg = avg + duration;
            } else {
                wrong = wrong + 1;
            }
            pb.inc();
        }
        (min, max, avg / (inputs.len() - wrong) as f64, wrong)
    }

}

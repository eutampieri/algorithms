use std::cmp::Ord;
use super::generic_sort::Sortable;

pub struct MergeSort<T: Ord> {
    storage: Vec<T>
}

impl<T: Ord + Clone> Sortable<T> for MergeSort<T> {
    fn sort(&mut self) -> &Vec<T> {
        self.storage = merge_sort(&self.storage);
        &self.storage
    }

    fn new(list: &Vec<T>) -> Self {
        Self{storage: list.clone()}
    }
}

fn merge<T: Ord + Clone>(left: Vec<T>, right: Vec<T>) -> Vec<T> {
    let mut res: Vec<T> = Vec::new();
    let mut left_iterator = left.iter();
    let mut right_iterator = right.iter();

    let mut l = left_iterator.next();
    let mut r = right_iterator.next();
    loop {
        if l.is_none() && r.is_none() {
            break;
        } else if l.is_none() {
            res.push(r.unwrap().clone());
            r = right_iterator.next();
        } else if r.is_none() {
            res.push(l.unwrap().clone());
            l = left_iterator.next()
        } else {
            let left_val = l.unwrap();
            let right_val = r.unwrap();
            if left_val < right_val {
                res.push(left_val.clone());
                l = left_iterator.next();
            } else {
                res.push(right_val.clone());
                r = right_iterator.next();
            }
        }
    }
    res
}
fn merge_sort<T: Ord + Clone>(list: &[T]) -> Vec<T> {
    if list.len() == 1 {
        return Vec::from(list);
    }
    let middle_point = list.len()/2;
    merge(merge_sort(&list[0 .. middle_point]), merge_sort(&list[middle_point..]))
}